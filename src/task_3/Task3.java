package task_3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Введите 3 числа");
        System.out.print("a = ");
        int a = in.nextInt();
        System.out.print("b = ");
        int b = in.nextInt();
        System.out.print("c = ");
        int c = in.nextInt();

        if (a > b) System.out.println(a);
        else if (b > c) System.out.println(b);
        else if (a > c) System.out.println(a);
        else System.out.println(c);

        System.out.print("Метод Math.max: ");
        System.out.println(Math.max(Math.max(a, b), c));
    }
}
