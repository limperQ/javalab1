package task_2;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите значение для угла: ");
        int angle = in.nextInt();

        int normalizeAngle = (angle % 360);

        System.out.println("Метод с '%'");
        if ((normalizeAngle >= 0)) {
            System.out.println("Ваш угол: angle = " + normalizeAngle);
        } else {
            System.out.println("Ваш угол: angle = " + (+normalizeAngle + 360));
        }
        System.out.println();
        System.out.println("Метод с floorMod");
        System.out.print("Ваш угол: angle = ");
        System.out.print(Math.floorMod(angle, 360));
    }
}
