package task_1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите 3 целочисленных числа.");
        System.out.print("1: ");
        int firstNumber = in.nextInt();
        System.out.print("2: ");
        int secondNumber = in.nextInt();
        System.out.print("3: ");
        int thirdNumber = in.nextInt();

        System.out.print("Первое число: " + Integer.toBinaryString(firstNumber) + " ");
        System.out.print(Integer.toOctalString(firstNumber) + " ");
        System.out.print(Integer.toHexString(firstNumber));
        System.out.println();
        System.out.print("Второе число: " + Integer.toBinaryString(secondNumber) + " ");
        System.out.print(Integer.toOctalString(secondNumber) + " ");
        System.out.print(Integer.toHexString(secondNumber));
        System.out.println();
        System.out.print("Третье число: " + Integer.toBinaryString(thirdNumber) + " ");
        System.out.print(Integer.toOctalString(thirdNumber) + " ");
        System.out.print(Integer.toHexString(thirdNumber));
        System.out.println();
        System.out.println(Double.toHexString(firstNumber));
        System.out.println(Double.toHexString(secondNumber));
        System.out.println(Double.toHexString(thirdNumber));
    }
}
